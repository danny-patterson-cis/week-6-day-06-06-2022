import React, { useState } from 'react'
import { Formik, Form, Field, ErrorMessage, FieldArray, FastField } from 'formik'
import * as Yup from 'yup'
import { TextError } from './TextError'


const initialValues = {
    fname: "",
    lname: "",
    email: "",
    comments: "",
    address: "",
    social: {
        facebook: "",
        twitter: ""
    },
    phoneNumbers: ["", ""],
    phNumbers: [""]
}

const saveData = {
    fname: "Divyang",
    lname: "Patel",
    email: "P@gmail.com",
    comments: "hello",
    address: "India",
    social: {
        facebook: "",
        twitter: ""
    },
    phoneNumbers: ["", ""],
    phNumbers: [""]
}

const onSubmit = (values, onSubmitProps) => {
    onSubmitProps.setSubmitting(false)
    onSubmitProps.resetForm()
    console.log("values", values);
}

const validationSchema = Yup.object({
    fname: Yup.string().required("Required!"),
    lname: Yup.string().required("Required"),
    email: Yup.string().email("Invalid Email format").required("Required "),
    comments: Yup.string().required("Required")

})

const validateComments = value => {
    let error
    if (!value) {
        error = "required"

    }
    return error
}

const FormInput = () => {
    const [formValues, setFormValues] = useState(null)

    return (
        <Formik
            initialValues={formValues || initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
            enableReinitialize
        // validateOnMount
        >

            {
                formik => {
                    return (
                        <Form >
                            <div className='mx-auto w-25 mt-3'>
                                <div className="mb-3">
                                    <label htmlFor="fname" className="form-label">First Name</label>
                                    <Field type="text"
                                        name="fname"
                                        className="form-control" id="fname" />
                                    <ErrorMessage name="fname" component={TextError} className='text-danger' />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="lname" className="form-label">Last Name</label>
                                    <Field type="text"
                                        name="lname"
                                        className="form-control"
                                        id="lname" />
                                    <ErrorMessage name="lname" component="div" className='text-danger' />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="email" className="form-label">Email ID</label>
                                    <Field type="email"
                                        name="email"
                                        className="form-control"
                                        id="email" />
                                    <ErrorMessage name="email">
                                        {
                                            errorMsg => <div className='text-danger'>{errorMsg}</div>
                                        }
                                    </ErrorMessage>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="comments" className="form-label">Comments</label>
                                    <Field component="textarea"
                                        name="comments"
                                        className="form-control"
                                        id="comments"
                                        validate={validateComments} />
                                    <ErrorMessage name="comments" component={TextError} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="address" className="form-label">Address</label>
                                    <FastField name="address" className="form-control">
                                        {props => {
                                            console.log("field render",)
                                            const { field, form, meta } = props

                                            return (
                                                <div>
                                                    <input className="form-control" type="text" id="address" {...field} />
                                                    {meta.touched && meta.error ? <div>{meta.error}</div> : null}
                                                </div>
                                            )
                                        }
                                        }
                                    </FastField>
                                    <div className='from-control'>
                                        <div className='from-control'>
                                            <label htmlFor="facebook" className="form-label">Facebook profile</label>
                                            <Field className="form-control" type="text" id="facebook" name="social.facebook" />
                                        </div>
                                        <div className='from-control'>
                                            <label htmlFor="twitter" className="form-label">Twitter profile</label>
                                            <Field className="form-control" type="text" id="twitter" name="social.twitter" />
                                        </div>
                                    </div>
                                    <div className='from-control'>
                                        <div className='from-control'>
                                            <label htmlFor="primaryPh" className="form-label">Primary phone number</label>
                                            <Field className="form-control" type="text" id="primaryPh" name="phoneNumbers[0]" />
                                        </div>
                                        <div className='from-control'>
                                            <label htmlFor="secondaryPh" className="form-label">Secondary phone number</label>
                                            <Field className="form-control" type="text" id="secondaryPh" name="phoneNumbers[1]" />
                                        </div>
                                    </div>
                                    <div className='from-control'>
                                        <label htmlFor="secondaryPh" className="form-label">List of phone number</label>
                                        <FieldArray className="form-label" name="phNumbers">
                                            {fieldArrayProps => {
                                                const { push, remove, form } = fieldArrayProps
                                                const { values } = form
                                                const { phNumbers } = values
                                                return <div className="form-label">
                                                    {
                                                        phNumbers.map((phNumber, index) => (
                                                            <div className="form-label " key={index}>
                                                                <Field className="form-label" name={`phNumbers[${index}]`} />
                                                                {
                                                                    index > 0 && (
                                                                        <button className="btn btn-primary" type="button" onClick={() => remove(index)}>
                                                                            {' '}
                                                                            -
                                                                            {' '}
                                                                        </button>)}
                                                                <button className="btn btn-primary" type="button" onClick={() => push("")}>{' '}
                                                                    +
                                                                    {' '}</button>
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            }
                                            }
                                        </FieldArray>
                                    </div>
                                </div>

                                <button type="submit" className="btn btn-primary" onClick={() => formik.validateField("comments")}>Validate Comments</button>
                                <button type="submit" className="btn btn-primary" onClick={() => formik.validateForm()}>validate All</button>
                                <button type="submit" className="btn btn-primary" onClick={() => setFormValues(saveData)}>Saved Values</button>
                                <button type="submit" className="btn btn-primary" onClick={() => formik.setTouched({
                                    fname: true,
                                    lname: true,
                                    email: true,
                                    comments: true,

                                })}>visit All</button>
                                <button type="submit" className="btn btn-primary" disabled={!formik.isValid || formik.isSubmitting}>Submit</button>
                                <button type="reset" className="btn btn-primary">Reset</button>
                            </div>
                        </Form>
                    )

                }
            }

        </Formik >
    )
}

export default FormInput
