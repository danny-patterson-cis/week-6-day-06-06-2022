import React from 'react'
import { useFormik } from 'formik'
import * as Yup from 'yup'

const initialValues = {
    fname: "",
    lname: "",
    email: ""
}

const onSubmit = values => {
    console.log("values", values);
}

const validationSchema = Yup.object({
    fname: Yup.string().required("Required!"),
    lname: Yup.string().required("Required"),
    email: Yup.string().email("Invalid Email format").required("Required ")

})
// const validate = values => {
//     let errors = {}
//     if (!values.fname) {
//         errors.fname = "Required"
//     }
//     if (!values.lname) {
//         errors.lname = "Required"
//     }
//     if (!values.email) {
//         errors.email = "Required"
//     } else if (!/^[A-Z-0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
//         errors.email = "invalid email format"
//     }

//     return errors;
// }

const OldForm = () => {
    const formik = useFormik({
        initialValues,
        onSubmit,
        // validate,
        validationSchema
    });
    console.log("errors", formik.errors);
    console.log("visited", formik.touched);

    return (
        <div className='mx-auto w-25 mt-3'>
            <form onSubmit={formik.handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="fname" className="form-label">First Name</label>
                    <input type="text"
                        name="fname"
                        className="form-control" id="fname"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.fname} />
                    {formik.touched.fname && formik.errors.fname ? <div className="text-danger">{formik.errors.fname}</div> : null}
                </div>
                <div className="mb-3">
                    <label htmlFor="lname" className="form-label">Last Name</label>
                    <input type="text"
                        name="lname"
                        className="form-control"
                        id="lname"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.lname} />
                    {formik.touched.lname && formik.errors.lname ? <div className="text-danger">{formik.errors.lname}</div> : null}
                </div>
                <div className="mb-3">
                    <label htmlFor="email" className="form-label">Email ID</label>
                    <input type="email"
                        name="email"
                        className="form-control"
                        id="email"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.email} />
                    {formik.touched.email && formik.errors.email ? <div className="text-danger">{formik.errors.email}</div> : null}
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>
    )
}

export default OldForm
