// import React from 'react'
// import { Field, field } from 'formik'
// import { Input, FromControl, FormLabel, FormErrorMessage } from '@chakra-ui/core'

// function ChakraInput(props) {
//     const { label, name, ...rest } = props
//     return (
//         <Field name={name}>
//             {
//                 ({ field, form }) => {
//                     return <FromControl isInvalid={form.errors[name] && form.touched[name]}>
//                         <FormLabel htmlFor={name}>{label}</FormLabel>
//                         <Input id={name} {...rest} {...field} />
//                         <FormErrorMessage>{form.errors[name]}</FormErrorMessage>
//                     </FromControl>
//                 }
//             }
//         </Field>
//     )
// }

// export default ChakraInput