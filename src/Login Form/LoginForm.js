import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import FormikControl from '../Formik/FormikControl'

function LoginForm() {
    const initialValues = {
        email: '',
        password: ''
    }
    const validationShchema = Yup.object({
        email: Yup.string().email('Invalid email format').required("required"),
        password: Yup.string().required("Required")
    })

    const onSubmit = values => {
        console.log("for", values)
    }
    return (
        <Formik initialValues={initialValues} validationSchema={validationShchema} onSubmit={onSubmit}>
            {
                formik => {
                    return <Form>
                        <FormikControl control='chakrainput' type='email' label='Email' name='email' />
                        <FormikControl control='chakrainput' type='password' label='Password' name='password' />
                        <button type="submit" disabled={!formik.isValid}>Submit</button>
                    </Form>
                }
            }
        </Formik>
    )
}

export default LoginForm