import './App.css';
import OldForm from './components/OldForm';
import FormInput from './components/FormInput';
import FormikContainer from './Formik/FormikContainer';
import LoginForm from './Login Form/LoginForm';
import RegistrastionForm from './Formik/RegistrastionForm';
import EnrollmentForm from './Formik/EnrollmentForm';
import { Theme, ThemeProvider } from '@chakra-ui/core'


function App() {
  return (
    // <ThemeProvider theme={Theme}>
    <div className="App">
      {/* <OldForm /> */}
      {/* <FormInput /> */}
      {/* <FormikContainer /> */}
      <LoginForm />
      {/* <RegistrastionForm /> */}
      {/* <EnrollmentForm /> */}
    </div>
    // </ThemeProvider>

  );
}

export default App;
